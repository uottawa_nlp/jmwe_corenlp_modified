# Steps to execute the code in your local computer 
1. Ensure you have Java 8 and ant.
2. Go into `CoreNLP/` directory.
3. Run the following commands, 
>
    ant 
    ant jar 
    bash runJMWE.sh
4. View the MWEs along with its frequencies in `data/twitter/mwe_frequencies.txt file`.
