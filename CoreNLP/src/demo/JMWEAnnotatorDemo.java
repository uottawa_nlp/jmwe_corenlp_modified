package demo;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import edu.mit.jmwe.data.IMWE;
import edu.mit.jmwe.data.IToken;
import edu.stanford.nlp.ling.CoreAnnotations.GenericTokensAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.JMWEAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.NamedEntityTagAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.PartOfSpeechAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.SentencesAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.TextAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.TokensAnnotation;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import edu.stanford.nlp.util.CoreMap;

/**
 * Demo to illustrate the jMWE Annotator.
 * @author Tomasz Oliwa
 *
 */

/**
  * Extended by Jinal Doshi and Dwarak Govind Parthiban
  */

public class JMWEAnnotatorDemo {

    public static void main(String[] args) throws IOException, IllegalArgumentException {
        String index = "lib/mweindex_wordnet3.0_semcor1.6.data";
        jmweStart(index);
    }

    /**
     * jMWE Demo, prints out discovered MWEs from the text
     * @param index the index
     * @param text the text
     */
    public static void jmweStart(String index) throws IOException, IllegalArgumentException {
        // creates the properties for Stanford CoreNLP: tokenize, ssplit, pos, lemma, jmwe
        Properties props = new Properties();
        props.setProperty("annotators", "tokenize, ssplit, pos, lemma, jmwe");
        props.setProperty("customAnnotatorClass.jmwe", "edu.stanford.nlp.pipeline.JMWEAnnotator");
        props.setProperty("customAnnotatorClass.jmwe.verbose", "false");
        props.setProperty("customAnnotatorClass.jmwe.underscoreReplacement", "-");
        props.setProperty("customAnnotatorClass.jmwe.indexData", index);
        props.setProperty("customAnnotatorClass.jmwe.detector", "CompositeConsecutiveProperNouns");
        StanfordCoreNLP pipeline = new StanfordCoreNLP(props);

        // Process io files
        File inputFile = new File("data/twitter/microblog2011_trimmed.txt");
        File outputFile = new File("data/twitter/mwe_frequencies.txt");
        BufferedReader reader = new BufferedReader(new FileReader(inputFile));
        BufferedWriter writer = new BufferedWriter(new FileWriter(outputFile));
        String line;
        List<String> corpus = new ArrayList<String>();

        while((line = reader.readLine()) != null ){
            corpus.add(line);
        }

        int corpusSize = corpus.size();
        Map<String, Integer> map = new HashMap<String, Integer>();
        int counter = 0;
        int batch = 50;

        // batching the sentences to speed up the computation time
        for(int i=0; i<corpusSize; i+=batch) {

            List<String> corpusBatch = corpus.
                subList(i,( (i+batch < corpusSize ) ? i+batch : corpusSize ) );
            String corpusBatchText = String.join(" . ", corpusBatch);

            // put the corpus in the document annotation
            Annotation doc = new Annotation(corpusBatchText);

            // run the CoreNLP pipeline on the document
            try{
                pipeline.annotate(doc);
            }
            catch(Exception e)
            {
                // Few exceptions occur due to incorrect tokenizations
                System.out.println(e);
            }


            // loop over the sentences
            List<CoreMap> sentences = doc.get(SentencesAnnotation.class);
            for(CoreMap sentence: sentences) {

              // loop over all discovered jMWE token and perform some action
              if(sentence.get(JMWEAnnotation.class) != null){
                  for (IMWE<IToken> token: sentence.get(JMWEAnnotation.class)) {

                        String mwe = token.getForm();
                        if(map.get(mwe) == null) {
                            map.put(mwe, 1);
                        }
                        else {
                            Integer freq = map.get(mwe);
                            map.put(mwe, freq + 1);
                        }
                    }
              }

            counter += 1;
            if(counter % 100 == 0) {
                System.out.println(counter + " sentences processed !");
            }

            }
        }

        map = MapUtil.sortByValue(map);

        for(Map.Entry<String, Integer> entry : map.entrySet()){
                writer.write(entry.getKey() + ":" + entry.getValue());
                writer.newLine();
        }

        reader.close();
        writer.close();

    }
}

/* https://stackoverflow.com/questions/109383/sort-a-mapkey-value-by-values-java/2581754#2581754
*/

class MapUtil {
    public static <K, V extends Comparable<? super V>> Map<K, V>
        sortByValue(Map<K, V> map) {
        List<Map.Entry<K, V>> list = new LinkedList<Map.Entry<K, V>>(map.entrySet());
        Collections.sort( list, new Comparator<Map.Entry<K, V>>() {
            public int compare(Map.Entry<K, V> o1, Map.Entry<K, V> o2) {
                return (o1.getValue()).compareTo( o2.getValue() );
            }
        });

        Map<K, V> result = new LinkedHashMap<K, V>();
        for (Map.Entry<K, V> entry : list) {
            result.put(entry.getKey(), entry.getValue());
        }
        return result;
    }
}
